int InputAnalogPin= 0;
int InputSensor= 0;
int Vin= 5;
float Vout= 0;
float KnownResistor= 1000;
float unKnownResistor= 0;
float buffer= 0;

void setup()
{
Serial.begin(9600);
}

void loop()
{
InputSensor= analogRead(InputAnalogPin);
buffer= InputSensor * Vin;
Serial.print("InputSensor: ");
Serial.println(InputSensor);
Vout= (buffer)/1023.0;
buffer= (Vin/Vout) -1;
unKnownResistor= KnownResistor * buffer;
Serial.print("Vout: ");
Serial.println(Vout);
Serial.print("unKnownResistor: ");
Serial.println(unKnownResistor);
delay(1000);
}
