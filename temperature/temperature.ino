/*
* Written by: Divine Abraham
* Purpose: Measure and Display current temperature as well as a list of temperature on LCD
*/
#include <LiquidCrystal.h>

const int temperaturePin = 0;
float fahrenheit;
const int piezopin = 9;
const int buttonpin1 = 7;
const int buttonpin2 = 6;
const int greenledPin =  13;
const int redledpin =  8;
int buttonState = 0;
int buttonState2 = 0;
float temp[10];
LiquidCrystal lcd (12, 11, 5, 4, 3, 2);

void setup()
{
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(buttonpin1, INPUT);
  pinMode(buttonpin2, INPUT);
  millis();
  pinMode(piezopin, OUTPUT);
  beep(50);
  beep(50);
  beep(50);
  delay(1000);
}

void looptemp()
{
 float temperature = getVoltage(temperaturePin);
 temperature = (temperature - .5) * 100; //Temperature in C
 fahrenheit = (temperature*1.8) + 32; //Temperature in F
 lcd.setCursor(0, 0);
 lcd.print("Temperature: ");
 lcd.setCursor(0, 1);
 lcd.print(temperature);
 lcd.print(" C");
 lcd.print(", ");
 lcd.print(fahrenheit);
 lcd.print(" F");
}

void loop()
{
buttonState2 = digitalRead(buttonpin2);
  if (buttonState2 == LOW)
    {
      digitalWrite(redledpin, HIGH);
      lcd.clear();
      if (temp [0]==NULL)
      {
        lcd.print("Temperature Data");
        lcd.setCursor(0,1);
        lcd.print("Empty");
        beep(600);
        beep(0);
        beep(300);
        delay(1000);
        beep(50);
        beep(50);
        beep(50);
        lcd.clear();
        looptemp();
      }
      else
      {
        for (int i = 0; i < 10; i++)
        {
          lcd.setCursor(0, 0);
          lcd.print(temp[i]);
          lcd.print(" C");
          beep(100);
          delay(1000);
        }
        lcd.clear();
        looptemp();
      }
  }
if (buttonState2 == HIGH)
    {
      digitalWrite(redledpin, LOW);
      looptemp();
    }
  buttonState = digitalRead(buttonpin1);
  if (buttonState == LOW)
    {
      digitalWrite(greenledPin, HIGH);
      lcd.clear();
      for (int i = 0; i < 10; i++)
        {
          digitalWrite(greenledPin, HIGH);
          lcd.setCursor(0, 0);
          lcd.print("");
          lcd.print(" Reading & Recording Data");
          beep(20);
          lcd.scrollDisplayLeft();
          float temperature = getVoltage(temperaturePin);
          temperature = (temperature - .5) * 100;
          temp [i] = temperature;
          delay(1000);
        }
        lcd.clear();
        looptemp();
      }
if (buttonState == HIGH)
    {
      digitalWrite(greenledPin, LOW);
      looptemp();
    }
}

float getVoltage(int tempin)
{
return (analogRead(tempin) * .004882814);//converting to voltage from 0-1023
}

void beep(unsigned char delayms)
{
  analogWrite(piezopin, 20);
  delay(delayms);
  analogWrite(piezopin, 0);
  delay(delayms);
}