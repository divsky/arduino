const int temperaturePin = 0; 
float fahrenheit;
const int inputPin1 = 7; 
const int inputPin2 = 6; 
const int ledPin =  13;
const int ledPin2 =  8;
int buttonState = 0;  
int buttonState2 = 0;  
unsigned long previousMillis = 0;        
long currentMillis = 0;
const long interval = 1000;
long current_time = 0;
long previous_time = 0;
const long interval2 = 200;
float temp[10];
#include  <LiquidCrystal.h>  
#define          sensor           0        
LiquidCrystal  lcd    (12, 11, 5, 4, 3, 2);
void  setup()
{
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(inputPin1, INPUT);
  pinMode(inputPin2, INPUT);
  millis();
}
void looptemp()
{
 float temperature = getVoltage(temperaturePin);
 temperature = (temperature - .5) * 100;
 fahrenheit = (temperature*1.8) + 32;
 lcd.setCursor(0, 0);
 lcd.print("Temperature: ");
 lcd.setCursor(0, 1);
 lcd.print(temperature);
 lcd.print(" C");
 lcd.print(", ");
 lcd.print(fahrenheit);
 lcd.print(" F");
}
void loop()
{
buttonState2 = digitalRead(inputPin2);
  if (buttonState2 == LOW)
  	{
    	digitalWrite(ledPin2, HIGH);
    	lcd.clear();
    	if (temp [0]==NULL)
    	{
    		lcd.print("Memory Empty");
    		delay(1000);
    	}
    	else 
    	{
    		for (int i = 0; i < 10; i++)
    		{
    			lcd.print(temp[i]);
    			lcd.print(",");
    			delay(1000);
    			lcd.scrollDisplayLeft();
    			lcd.scrollDisplayLeft();
    			lcd.scrollDisplayLeft();
    			lcd.scrollDisplayLeft();
    			lcd.scrollDisplayLeft();
    			lcd.scrollDisplayLeft();
    		}
    		lcd.clear();
    	}
	}
if (buttonState2 == HIGH)
  	{
    digitalWrite(ledPin2, LOW);
	}
unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) 
  	{
    previousMillis = currentMillis;
      looptemp();
  	}
  buttonState = digitalRead(inputPin1);
  if (buttonState == LOW)
  	{
    	digitalWrite(ledPin, HIGH);
    	for (int i = 0; i < 10; i++)
    		{
    			float temperature = getVoltage(temperaturePin);
 				temperature = (temperature - .5) * 100;
    			temp [i] = temperature;
    			digitalWrite(ledPin, HIGH);
    			lcd.setCursor(0, 0);
    			lcd.print("");
 				lcd.print(" Reading and Recording");
 				lcd.setCursor(0, 1);
    			lcd.print("                         ");
 				lcd.scrollDisplayLeft();
    			delay(1000);
    		}
    		lcd.clear();
	}
if (buttonState == HIGH)
  	{
    digitalWrite(ledPin, LOW);
	}
}
float getVoltage(int pin)
	{
 return (analogRead(pin) * .004882814);
	}

